#include "Esp32_Blink.h"

void app_main(void)
{
    Esp32_BlinkInit();

    while (1) {
        Esp32_BlinkMain();
    }
}
