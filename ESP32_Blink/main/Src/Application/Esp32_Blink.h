/*!
* \file Esp32_Blink.h
* \brief Brief description of the file
* \author Javier Morales
*
* Detailed description of the file
*/

#ifndef _ESP32_BLINK_H        /* Guard against multiple inclusion */
#define _ESP32_BLINK_H

//-----------------------------------------------------------------------------
/* Inclusions */
//-----------------------------------------------------------------------------

/* Standard libraries */

/* Manufacturer libraries */

/* Common libraries */

/* Specific libraries */

//-----------------------------------------------------------------------------
/* Constants definition */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Types and structs definition */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Exported function prototypes */
//-----------------------------------------------------------------------------
void Esp32_BlinkInit(void);
void Esp32_BlinkMain(void);

#endif /* _ESP32_BLINK_H */
//-----------------------------------------------------------------------------
/*!
 End of file
*/
