/*!
* \file Esp32_Gpio.h
* \brief Brief description of the file
* \author Javier Morales
*
* Detailed description of the file
*/

#ifndef _ESP32_GPIO_H        /* Guard against multiple inclusion */
#define _ESP32_GPIO_H

//-----------------------------------------------------------------------------
/* Inclusions */
//-----------------------------------------------------------------------------

/* Standard libraries */

/* Manufacturer libraries */

/* Common libraries */

/* Specific libraries */

//-----------------------------------------------------------------------------
/* Constants definition */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Types and structs definition */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Exported function prototypes */
//-----------------------------------------------------------------------------
void Esp32_GpioInit(void);
void Esp32_GpioToggle(void);

#endif /* _ESP32_GPIO_H */
//-----------------------------------------------------------------------------
/*!
 End of file
*/
