/*!
* \file Esp32_Gpio.c
* \brief Brief description of the file
* \author Javier Morales
*
* Detailed description of the file
*/

//-----------------------------------------------------------------------------
/* Inclusions */
//-----------------------------------------------------------------------------

/* Standard libraries */

/* Manufacturer libraries */
#include "driver/gpio.h"
#include "esp_log.h"

/* Specific libraries */
#include "Esp32_Gpio.h"

//-----------------------------------------------------------------------------
/* Constants definition */
//-----------------------------------------------------------------------------

/* ESP32-S3 Target selected*/
#if defined(CONFIG_IDF_TARGET_ESP32S3) && CONFIG_IDF_TARGET_ESP32S3 == 1
    #define ui8GPIO_LED 14

/* ESP32-C3 Target selected*/
#elif defined(CONFIG_IDF_TARGET_ESP32C3) && CONFIG_IDF_TARGET_ESP32C3 == 1
    #define ui8GPIO_LED 19

/* ESP32 Target selected*/
#elif defined(CONFIG_IDF_TARGET_ESP32) && CONFIG_IDF_TARGET_ESP32 == 1
    #define ui8GPIO_LED 2

#endif

#define TAG "Esp32_Gpio"

//-----------------------------------------------------------------------------
/* Types and structs definition */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Non-exported function prototypes */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/* Global variables */
//-----------------------------------------------------------------------------
uint8_t Gui8LedStatus;

//-----------------------------------------------------------------------------
/* Body functions */
//-----------------------------------------------------------------------------

void Esp32_GpioInit(void)
{
    /* Set the GPIO as a push/pull output */
    gpio_reset_pin(ui8GPIO_LED);
    gpio_set_direction(ui8GPIO_LED, GPIO_MODE_OUTPUT);
    
    Gui8LedStatus = 0;  

    ESP_LOGI(TAG, "Gpio driver initialized");
}

void Esp32_GpioToggle(void)
{
    gpio_set_level(ui8GPIO_LED, Gui8LedStatus);
    Gui8LedStatus = !Gui8LedStatus;

    ESP_LOGI(TAG, "Toggling integrated LED!");
}

//-----------------------------------------------------------------------------
/*!
 End of file
*/
